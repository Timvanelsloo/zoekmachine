var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
    host: 'localhost:9200',
    log: 'trace'
});

client.search({
    index: 'se',
    body: {
        query: {
            filtered: {
                query: {
                    multi_match: {
                        query: 'boterzachte bezuinigingen',
                        fields: ['subject', 'text']
                    }
                },
                filter: {
                    bool: {
                        must: [{
                            term: {
                                'party': 'pvda'
                            }
                        }]
                    }
                }
            }
        },
        aggregations: {
            by_distinct: {
                terms: {
                    field: 'party'
                },
                aggregations: {
                    tops: {
                        top_hits: {
                            size: 1
                        }
                    }
                }
            }
        }
    }
}, function(err, response) {
    console.log(response.hits.hits.map(function(x) {
        return x._source;
    }));
});
