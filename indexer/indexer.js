var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
    host: 'localhost:9200',
    log: 'trace'
});

client.indices.delete({
    'index': '*'
}, function(err) {
    client.indices.create({
        index: 'se',
        body: {
            'mappings': {
                'speech': {
                    'properties': {
                        'party': {
                            'type': 'string',
                            'index': 'not_analyzed'
                        },
                        'speaker_raw': {
                            'type': 'string',
                            'index': 'not_analyzed'
                        },
                        'subject_raw': {
                            'type': 'string',
                            'index': 'not_analyzed'
                        }
                    }
                }
            }
        }
    }, function(err) {
        var fs = require('fs'),
            xml = require('libxmljs');

        /* Start by reading the list of data files. */
        var files = fs.readdirSync('data');

        files.forEach(function(file) {
            if (file.indexOf('.xml') < 0)
                return;

            file = 'data/' + file;

            var doc = xml.parseXml(fs.readFileSync(file).toString().replace(/xmlns/g, "x").replace(/folia:/g, "").replace(/dc:/g, "").replace(/pm:/g, ""));

            var speeches = doc.find('//speech'),
                title = doc.get('//title').text();

            console.log(title);

            var parties = doc.find('//speech/@party').map(function(x) {
                return x.value();
            });

            parties = parties.filter(function(x, i) {
                return parties.indexOf(x) == i;
            }).map(function(x) { return x.toLowerCase(); }).filter(function(x) {
                return x && x.length > 0;
            });

            speeches.forEach(function(speech) {
                var words = speech.find('./w');

                var full = speech.find('.//s').map(function(x) {
                    return [
                        x.get('.//t[1]').text(),
                    ];
                });

                full = speech.find('.//s').map(function(x) {
                    return x.get('.//t[1]').text();
                }).join('\n').replace(/ ([\.\?,!;:\)\(])/g, '$1');

                var speaker = {
                    name: speech.attr('speaker') ? speech.attr('speaker').value().toLowerCase() : null,
                    party: speech.attr('party') ? speech.attr('party').value().toLowerCase() : null,
                };

                console.log('\n\n' + speaker.name + ' (' + speaker.party + '):');
                console.log(full);

                title = title.replace(/^(.*)\((.*)\)(.*)\((.*)\)(.*)$/, '$2')

                client.index({
                    index: 'se',
                    type: 'speech',
                    body: {
                        speaker: speaker.name,
                        speaker_raw: speaker.name,
                        party: speaker.party,
                        subject: title,
                        subject_raw: title,
                        text: full,
                        parties: parties
                    }
                });
            });
        });
    });
});
