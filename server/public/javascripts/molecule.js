window.onload = function() {
    var div = document.getElementById('molecule-field');
    div.contentEditable = true;
    div.style.padding = '10px';
    div.style.fontSize = '24px';

    var match = function (text, callback) {
        text = text.replace(new RegExp(String.fromCharCode(160), "g"), ' ');

        var cb = 'callback_' + Math.floor(Math.random() * 256 * 256 * 256 * 256);
        var url = 'suggest?q=' + encodeURIComponent(text) + '&cb=' + cb;
        window[cb] = callback;

        var script = document.createElement('script');
        script.src = url;
        document.body.appendChild(script);
    };

    div.addEventListener('input', function() {
        console.log(this.innerHTML);
        if (this.innerHTML.indexOf('<br') >= 0 ||
            this.innerHTML.indexOf('<div') >= 0) {

            this.innerHTML = this.innerHTML.replace(/<(br|div|\/div)([^>]*)>/g, '');
            // this.blur();

            search(this.innerText);
        }

        var text = this.innerText.replace(/\n/g, '');

        match(text, function(err, results) {
            console.log(results);
            var selection = saveSelection(this);

            var html = text.split('').map(function(x) {
                var span = document.createElement('span');
                span.innerText = x;
                return span;
            }),
                offset = 0;

            this.innerText = '';

            results.forEach(function(x) {
                var slice = html.slice(x.start, x.end);

                slice.forEach(function(char) {
                    char.style.color = 'rgb(' + x.match.color.join(', ') + ')';
                    char.style.backgroundColor = 'rgba(' + x.match.color.join(', ') + ', .1)';
                    char.style.borderBottom = '2px solid rgb(' + x.match.color.join(', ') + ')';

                    if (x.match.type == 'strikethrough')
                        char.style.textDecoration = 'line-through';
                });
            });

            html.forEach(function(element) {
                this.appendChild(element);
            }.bind(this));

            restoreSelection(this, selection);
        }.bind(this));
    });

    div.focus();
};
