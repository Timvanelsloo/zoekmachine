var search = function search(text) {
    text = text.replace(new RegExp(String.fromCharCode(160), "g"), ' ');

    if (text.length == 0)
        return;

    var cb = 'callback_' + Math.floor(Math.random() * 256 * 256 * 256 * 256);
    var url = 'search?q=' + encodeURIComponent(text) + '&cb=' + cb;
    window[cb] = function(err, result) {
        var ul = document.getElementById('suggestions-list');
        ul.innerHTML = '';

        var topics = result.suggestions;
        topics.forEach(function(topic) {
            var num = Math.min(4, topics.length);

            var li = document.createElement('li');
            li.style.width = (100 / num).toFixed(2) + '%';
            li.innerText = topic;
            ul.appendChild(li);

            li.onclick = function() {
                var div = document.getElementById('molecule-field');

                div.innerText = div.innerText += ' ' + li.innerText + '\n';
                div.dispatchEvent(new Event('input'));
            };
        });

        var div = document.getElementById('results');
        div.innerHTML = '';

        result.results.forEach(function(item) {
            var d = document.createElement('div');

            var h1 = document.createElement('h1');
            h1.innerText = item['subject'];
            d.appendChild(h1);

            var ul = document.createElement('ul');
            ul.className = 'parties';

            var parties = item.parties;

            parties.forEach(function(x) {
                var li = document.createElement('li');
                li.innerText = x;
                ul.appendChild(li);
            });

            d.appendChild(ul);

            item.speeches.forEach(function(speech) {
                var dd = document.createElement('div');
                dd.className = 'speech';

                var h2 = document.createElement('strong');

                if (speech.party)
                    h2.innerText = speech.speaker + ' (' + speech.party + ')';
                else
                    h2.innerText = speech.speaker;

                h2.innerText += ':';

                dd.appendChild(h2);

                var p = document.createElement('span');
                p.innerHTML = "‟" + speech.text + "„";
                dd.appendChild(p);

                d.appendChild(dd);
            });

            div.appendChild(d);
        });
    };

    var script = document.createElement('script');
    script.src = url;
    document.body.appendChild(script);
};
