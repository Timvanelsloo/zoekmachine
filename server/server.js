var express = require('express');
var app = express();

var annotate = require('./lib/annotate');

app.use(express.static(require('path').join(__dirname, 'public')));

app.get('/suggest', function(req, res) {
    res.send(req.query['cb'] + '(null, ' + JSON.stringify(annotate(req.query['q'])) + ');');
});

var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
    host: 'localhost:9200',
    log: 'trace'
});

/* Load all unique parties, speakers, subjects and stopwords. */
client.search({ index: 'se', body: { aggs: {
    parties:  { terms: { field: "party", size: 0 } },
    speakers: { terms: { field: "speaker_raw", size: 0 } },
    stopwords: { terms: { field: "text", size: 100 } },
    subjects: { terms: { field: "text", size: 0, min_doc_count: 5, order: { _count: 'asc' } } }
} } }, function(err, response) {
    annotate.types.party = response.aggregations.parties.buckets.map(function(x) {
        return x.key;
    });
    annotate.types.person = response.aggregations.speakers.buckets.map(function(x) {
        return x.key;
    });
    annotate.types.stopword = response.aggregations.stopwords.buckets.map(function(x) {
        return x.key;
    }).filter(function(x) {
        return x != 'en' && x != 'of';
    });
    annotate.types.subject = response.aggregations.subjects.buckets.filter(function(x) {
        return x.doc_count < 500 && x.key.length > 1;
    }).map(function(x) {
        return x.key;
    }).filter(function(x) {
        return annotate.types.party.indexOf(x) < 0;
    });
});

app.get('/search', function(req, res) {
    var q = req.query['q'].replace(/(^\s+|\s+$)/g, '');

    var itm = annotate(q);

    var bool = {
        must: [

        ]
    };

    console.log(itm);

    for (var i = itm.length - 1; i >= 0; i --) {
        if (itm[i].match.type == 'party' ||
            itm[i].match.type == 'stopword' ||
            itm[i].match.type == 'combinator') {
            q = q.substr(0, itm[i].start) + q.substr(itm[i].end);
        }

        if (itm[i].match.type == 'party') {
            bool.must.push({
                term: {
                    parties: itm[i].match.candidate
                }
            });
        }else if (itm[i].match.type == 'combinator' &&
                  itm[i].match.candidate == ' of ' &&
                  i >= 1 && itm[i - 1].match.type == 'party' &&
                  bool.must.length > 0) {
          q = q.substr(0, itm[i - 1].start) + q.substr(itm[i - 1].end);
            bool.must[bool.must.length - 1] = {
                or: {
                    filters: [
                        bool.must[bool.must.length - 1],
                        { term: { parties: itm[i - 1].match.candidate } }
                    ]
                }
            };

            i --;
        }
    }

    var query = {
        match_all: {}
    };

    console.log(bool.must);

    if (q.length > 0) {
        query = {
            multi_match: {
                query: q,
                fields: ['subject', 'text', 'speaker']
            }
        };
    }

    client.search({
        index: 'se',
        body: {
            query: {
                filtered: {
                    query: query,
                    filter: {
                        bool: bool
                    }
                }
            },
            aggs: {
                by_distinct: {
                    terms: {
                        field: 'subject_raw',
                        size: 0
                    },
                    aggs: {
                        tops: {
                            top_hits: {
                                size: 5,
                                highlight: {
                                    fields: {
                                        text: {
                                            fragment_size: 150,
                                            number_of_fragments: 3
                                        },
                                        speaker: {}
                                    }
                                }
                            }
                        }
                    }
                },
                sigs: {
                    significant_terms: { "field": 'text' }
                }
            }
        }
    }, function(err, response) {
        var buckets = response.aggregations.by_distinct.buckets;
        res.send(req.query['cb'] + '(null, ' + JSON.stringify({
            suggestions: response.aggregations.sigs.buckets.map(function(x) {
                return x.key
            }).filter(function(x) {
                return q.split(' ').indexOf(x) < 0
            }),
            results: buckets.map(function(x) {
                return {
                    subject: x.key,
                    parties: (x.tops.hits.hits[0] ? x.tops.hits.hits[0]._source.parties : []),
                    speeches: x.tops.hits.hits.filter(function(y) {
                        return y.highlight;
                    }).map(function(y) {
                        y._source.text = (y.highlight.text || y._source.text.split('\n').slice(0, 3)).join(' ... ');
                        return y._source;
                    })
                };
            })
        }) + ');');
    });
});

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});
