var types = {
    'party': [],
    'subject': [],
    'person':  [],
    'year': [],
    'stopword': []
};

var i;
for (i = 1900; i <= 2100; i ++) {
    types.year.push(i.toString());
}

var colors = {
    'party': [255, 32, 0],
    'subject': [0, 127, 255],
    'person': [127, 32, 255],
    'year': [255, 32, 127],
    'stopword': [192, 192, 192]
};

var search = function search(string) {
    var results = [];

    for (var type in module.exports.types) {
        types[type].forEach(function(candidate) {
            if (candidate.indexOf(string.toLowerCase()) >= 0) {
                results.push({
                    type: type,
                    candidate: candidate,
                    exact: string.toLowerCase() == candidate,
                    color: colors[type]
                });
            }
        });
    }

    return results;
};

var annotate = function annotate(string) {
    var annotations = [];

    for (var i = 0; i < string.length; i ++) {
        var next = string.substr(i).indexOf(' ');

        var sub = string.substr(i, next >= 0 ? next : string.length - i);

        var results = search(sub);

        var lastExact = null;
        var success = false;

        var si = i;

        while (results.length >= 1) {
            results.forEach(function(r) {
                if (r.exact) {
                    lastExact = {
                        start: si,
                        end:  i + sub.length,
                        match: r
                    };
                }
            });

            if (results.length == 1 && results[0].exact) {
                annotations.push({
                    start: si,
                    end:  i + sub.length,
                    match: results[0]
                });
                success = true;
                break;
            }else {
                if (next < 0)
                    break;

                i += next + 1;
                next = string.substr(i).indexOf(' ');

                if (next <= 0)
                    next = string.substr(i).length;

                sub += ' ' + string.substr(i, next);

                results = search(sub);
            }
        }

        if (!success && lastExact) {
            annotations.push(lastExact);
            i = lastExact.start;
            next = string.substr(i).indexOf(' ');
        }else if (!success) {
            i = si;
            next = string.substr(i).indexOf(' ');
        }

        if (next < 0)
            break;

        i += next;
    }

    /* Now go through string once more to find combinators (or, and). */

    for (var i = 0; i < string.length; i ++) {
        var next = string.substr(i).indexOf(' ');

        var sub = string.substr(i, next >= 0 ? next : string.length - i);

        if (sub == 'en' || sub == 'of') {
            var start = null, end = null;

            annotations.forEach(function(x) {
                if (x.end + 1 == i)
                    start = x;
                else if (x.start - 1 == i + sub.length)
                    end = x;
            });

            if (start && end &&
                start.match.type == end.match.type && start.match.type == 'party') {
                annotations.push({
                    start: i - 1,
                    end: i + sub.length + 1,
                    match: {
                        type: 'combinator',
                        candidate: ' ' + sub + ' ',
                        exact: true,
                        color: [0, 0, 0]
                    }
                });
            }else if (start && end) {
                annotations.push({
                    start: i - 1,
                    end: i + sub.length + 1,
                    match: {
                        type: 'strikethrough',
                        candidate: ' ' + sub + ' ',
                        exact: true,
                        color: [192, 192, 192],
                        strikethrough: true
                    }
                });
            }
        }

        if (next < 0)
            break;

        i += next;
    }

    return annotations.sort(function(a, b) {
        return a.start - b.start;
    });
};

annotate.types = types;

console.log(annotate('pvda en vvd willen betere economie van der a'));

module.exports = annotate;
